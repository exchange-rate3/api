﻿namespace Infrastructure
{
    using Application;
    using Application.Repositories;

    public class UnitOfWork(ExchangeRatesDbContext exchangeRatesDbContext, IExchangeRateRepository exchangeRateRepository) : IUnitOfWork
    {
        private readonly ExchangeRatesDbContext _exchangeRatesDbContext = exchangeRatesDbContext;

        public IExchangeRateRepository ExchangeRateRepository { get; } = exchangeRateRepository;

        public Task SaveChanges(CancellationToken cancellationToken)
        {
            return _exchangeRatesDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
