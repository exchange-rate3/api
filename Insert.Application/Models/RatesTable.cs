﻿namespace Application.Models
{
    public class RatesTable
    {
        public required string Table { get; set; }

        public required string No { get; set; }

        public string? TradingDate { get; set; }

        public required string EffectiveDate { get; set; }

        public required IEnumerable<Rate> Rates { get; set; }
    }
}
