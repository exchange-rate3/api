﻿namespace Application.Services
{
    using Application.Models;

    public interface INbpApiService
    {
        Task<IEnumerable<RatesTable>> GetAllCurrencies(CancellationToken cancellationToken);
    }
}
