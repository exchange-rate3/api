﻿namespace Application.Models
{
    public record ExchangeRate
    {
        public required string Currency { get; set; }

        public required string Code { get; set; }

        public required decimal Mid { get; set; }

        public required DateOnly EffectiveDate { get; set; }
    }
}
