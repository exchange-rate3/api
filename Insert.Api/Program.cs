using Application;
using Application.Repositories;
using Application.Services;
using Hangfire;
using Infrastructure;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Insert.Infrastructure.Jobs;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var globalConfiguration = builder.Configuration;

builder.Services.AddHangfire(configuration => configuration
    .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
    .UseSimpleAssemblyNameTypeSerializer()
    .UseRecommendedSerializerSettings()
    .UseSqlServerStorage(globalConfiguration.GetConnectionString("HangfireConnection")));

builder.Services.AddHangfireServer();
builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IExchangeRateRepository, ExchangeRateRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IFetchCurrencyService, FetchCurrencyService>();

builder.Services.AddHttpClient<INbpApiService, NbpApiService>(client =>
{
    client.BaseAddress = new Uri("http://api.nbp.pl/api/exchangerates/");
    client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
});

builder.Services.AddDbContext<ExchangeRatesDbContext>(
    options => options.UseSqlServer(globalConfiguration.GetConnectionString("ExchangeRatesConnection")));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    using (var scope = app.Services.CreateScope())
    {
        var db = scope.ServiceProvider.GetRequiredService<ExchangeRatesDbContext>();
        db.Database.Migrate();
    }

    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.DisplayRequestDuration();
    });
}
app.UseRouting();

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseHangfireDashboard();
RecurringJob.AddOrUpdate<CurrencySynchronizerJob>(
    nameof(CurrencySynchronizerJob),
    (job) => job.Synchronize(default),
    globalConfiguration.GetRequiredSection("SynchronizeCurrenciesJob").Value);
RecurringJob.TriggerJob(nameof(CurrencySynchronizerJob));

app.MapControllers();

app.Run();
