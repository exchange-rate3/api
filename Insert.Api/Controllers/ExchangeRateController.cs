namespace Insert.Controllers
{
    using Application.Models;
    using Application.Services;
    using Hangfire;
    using Hangfire.Storage;
    using Insert.Infrastructure.Jobs;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("ExchangeRates")]
    [Produces("application/json")]
    public class ExchangeRateController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ExchangeRate>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Get([FromServices] IFetchCurrencyService currencyService, CancellationToken cancellationToken)
        {
            var result = await currencyService.Fetch(cancellationToken);

            if (result.Any() is false)
                return NoContent();

            return Ok(result);
        }

        [HttpGet("LastSynchronizationDate")]
        [ProducesResponseType(typeof(DateTime), StatusCodes.Status200OK)]
        public IActionResult GetLastSynchronizationDate([FromServices] JobStorage jobStorage)
        {
            var lastJobExecution = jobStorage.GetReadOnlyConnection().GetRecurringJobs(new[] { nameof(CurrencySynchronizerJob) }).First().LastExecution;

            return Ok(lastJobExecution);
        }
    }
}
