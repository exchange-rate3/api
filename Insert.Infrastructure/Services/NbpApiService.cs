﻿namespace Infrastructure.Services
{
    using System.Net.Http;
    using System.Net.Http.Json;
    using Application.Models;
    using Application.Services;

    public class NbpApiService(HttpClient nbpClient) : INbpApiService, IDisposable
    {
        private readonly HttpClient _nbpClient = nbpClient;

        public async Task<IEnumerable<RatesTable>> GetAllCurrencies(CancellationToken cancellationToken)
        {
            var tableA = _nbpClient.GetFromJsonAsync<IEnumerable<RatesTable>>($"tables/a", cancellationToken);
            var tableB = _nbpClient.GetFromJsonAsync<IEnumerable<RatesTable>>($"tables/b", cancellationToken);

            var completedTasks = await Task.WhenAll(tableA, tableB);
            var result = new List<RatesTable>();

            result.AddRange(completedTasks[0]!);
            result.AddRange(completedTasks[1]!);

            return result;
        }

        public void Dispose()
        {
            _nbpClient?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
