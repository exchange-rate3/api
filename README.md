# Insert

## Wymagania

1. Node.js 18.17 or later.
1. Baza SQL Server, np. SQLEXPRESS.
   1. Stowrzyć bazę dla Hangfire:
      ```sql
      CREATE DATABASE [HangfireDb]
      ```
   1. Zweryfikuj ConnectionStringi w plikach appsettings.

## Uruchomienie

### API

1. Aplikację można uruchomić przez Visual Studio lub przez CLI `dotnet run` będąc w folderze `Insert.Api`.

### UI

1. Zainstalować zależności za pomocą `npm ci`.
1. Zweryfikować URL dla API w pliku `next.config.js`.
1. Uruchomić aplikację za pomocą `npm run dev`.
1. Przejść pod adres podany w terminalu.
