﻿namespace Application
{
    using Application.Repositories;

    public interface IUnitOfWork
    {
        IExchangeRateRepository ExchangeRateRepository { get; }

        Task SaveChanges(CancellationToken cancellationToken);
    }
}
