﻿namespace Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models;
    using Application.Repositories;
    using Microsoft.EntityFrameworkCore;

    public class ExchangeRateRepository(ExchangeRatesDbContext exchangeRatesDbContext) : IExchangeRateRepository
    {
        private readonly ExchangeRatesDbContext _exchangeRatesDbContext = exchangeRatesDbContext;

        public void Add(ExchangeRate item)
        {
            _exchangeRatesDbContext.Add(item);
        }

        public async Task<IDictionary<string, ExchangeRate>> GetAll(CancellationToken cancellationToken)
        {
            return await _exchangeRatesDbContext.ExchangeRates.ToDictionaryAsync(item => item.Code, cancellationToken);
        }
    }
}
