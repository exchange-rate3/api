﻿namespace Application.Repositories
{
    using System.Collections.Generic;
    using Application.Models;

    public interface IExchangeRateRepository
    {
        void Add(ExchangeRate item);

        Task<IDictionary<string, ExchangeRate>> GetAll(CancellationToken cancellationToken);
    }
}
