﻿namespace Infrastructure
{
    using Application.Models;
    using Microsoft.EntityFrameworkCore;

    public class ExchangeRatesDbContext(DbContextOptions<ExchangeRatesDbContext> options) : DbContext(options)
    {
        public DbSet<ExchangeRate> ExchangeRates { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ExchangeRate>()
                .HasKey(r => r.Code);

            builder.Entity<ExchangeRate>()
                .Property(r => r.Code)
                .HasMaxLength(3);

            builder.Entity<ExchangeRate>()
                .Property(r => r.Currency)
                .HasMaxLength(256);

            builder.Entity<ExchangeRate>()
                .Property(r => r.Mid)
                .HasColumnType("decimal(13,10)");
        }
    }
}
