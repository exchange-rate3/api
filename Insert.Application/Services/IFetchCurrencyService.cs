﻿namespace Application.Services
{
    using Application.Models;

    public interface IFetchCurrencyService
    {
        Task<IEnumerable<ExchangeRate>> Fetch(CancellationToken cancellationToken);
    }
}
