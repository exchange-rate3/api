﻿namespace Application.Models
{
    public class Rate
    {
        public required string Currency { get; set; }

        public required string Code { get; set; }

        public required decimal Mid { get; set; }
    }
}
