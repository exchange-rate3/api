﻿namespace Infrastructure.Services
{
    using Application;
    using Application.Models;
    using Application.Services;

    public class FetchCurrencyService(IUnitOfWork unitOfWork) : IFetchCurrencyService
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<IEnumerable<ExchangeRate>> Fetch(CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.ExchangeRateRepository.GetAll(cancellationToken);

            return result.Values;
        }
    }
}
