﻿namespace Insert.Infrastructure.Jobs
{
    using System.Globalization;
    using Application;
    using Application.Models;
    using Application.Services;

    public class CurrencySynchronizerJob(INbpApiService nbpApiService, IUnitOfWork unitOfWork)
    {
        private readonly INbpApiService _nbpApiService = nbpApiService;
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task Synchronize(CancellationToken cancellationToken)
        {
            var nbpResultTask = _nbpApiService.GetAllCurrencies(cancellationToken);
            var currentRatesTask = _unitOfWork.ExchangeRateRepository.GetAll(cancellationToken);
            await Task.WhenAll(nbpResultTask, currentRatesTask);

            var nbpResult = await nbpResultTask;
            var currentRates = await currentRatesTask;

            var exchangeRates = nbpResult.SelectMany(
                table => table.Rates,
                (table, rate) => new ExchangeRate
                {
                    Code = rate.Code,
                    Currency = rate.Currency,
                    Mid = rate.Mid,
                    EffectiveDate = DateOnly.Parse(table.EffectiveDate, CultureInfo.InvariantCulture)
                });

            foreach (var exchangeRate in exchangeRates)
            {
                if (currentRates.TryGetValue(exchangeRate.Code, out var rate))
                {
                    rate.Currency = exchangeRate.Currency;
                    rate.Mid = exchangeRate.Mid;
                    rate.EffectiveDate = exchangeRate.EffectiveDate;
                }
                else
                {
                    _unitOfWork.ExchangeRateRepository.Add(exchangeRate);
                }
            }

            await _unitOfWork.SaveChanges(cancellationToken);
        }
    }
}
